import helloWorld from '../src/hello';

describe('Hello Unit Test', () => {
  it('says "Hello world!"', () => {
    expect(helloWorld()).toEqual('Hello world!');
  });
});
