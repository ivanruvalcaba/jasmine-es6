# Jasmine ES6

## Overview

This project has the purpose of provide a quick and basic starting point for using Jasmine Testing Framework with **ECMAScript 6 (ES6)** support.

## What's included

+ **Babel (ver. 6.26.0)** — ES6 to ES5 transpoiler (ES6 support).
+ **Jasmine (ver. 3.2.0)** — BDD Testing Framework.

### Folder structure

```sh
jasmine-es6
├── LICENSE
├── package.json
├── README.md
├── spec
│   ├── helloSpec.js
│   ├── run.js
│   └── support
│       └── jasmine.json
└── src
    └── hello.js
```

## Install

```sh
# Clone the repository
git clone https://gitlab.com/ivanruvalcaba/jasmine-es6.git

# Go into the repository
cd jasmine-es6
```

Then, proceed to install the dependencies by choosing one of the options shown below.

### Install dependencies

#### npm

```sh
npm install
```

or

#### yarn (recommended)

> Requires package installation. Follow the instructions given [here](https://yarnpkg.com/lang/en/docs/install/#debian-stable).

```sh
yarn install
```

## How to use?

This project works _out-of-the-box_, following the guidelines that Jasmine establishes in its [documentation](https://jasmine.github.io/tutorials/your_first_suite). Place your _script files_ inside the `src` directory (for example: `hello.js`), and your _spec files_ inside the `spec` directory (for example: `helloSpec.js`).

> **Note:** The following command requires its execution every time that some modification is made in the files of the project in which you're working.

Then, execute `yarn test` or `npm test` according to the case.

### Output

Below is a output sample:

```sh
yarn run v1.10.1
$ babel-node spec/run.js
Started
.


1 spec, 0 failures
Finished in 0.062 seconds
Done in 12.37s.
```

## Configuring

Check the following files if you want to customize the directory tree structure or nomenclature of the spec files as well as some minimal configurations.

+ `spec/support/jasmine.json` — Jasmine configuration file.
+ `spec/run.js` — Path to the Jasmine configuration file.

## License

This project is subject to the terms of the [GNU General Public License, Version 3](https://www.gnu.org/licenses/gpl-3.0.en.html).
